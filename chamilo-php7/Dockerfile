#

FROM php:7.2-apache

ARG CHAMILO_VERSION=1.11.6
ARG CHAMILO_TAR="chamilo-${CHAMILO_VERSION}-php7.tar.gz"

RUN apt-get -y update && apt-get install -y \
    apt-utils \
    curl \
    git \
    wget \
    zlib1g-dev \
    libpng-dev \
    libicu-dev \
    g++

RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl && \
    docker-php-ext-install -j$(nproc) gd && \
    docker-php-ext-install opcache && \
    docker-php-ext-install pdo_mysql && \
    docker-php-ext-install zip

# Install APCu
RUN pecl install apcu && \
    echo "extension=apcu.so" > /usr/local/etc/php/conf.d/apcu.ini

# download and install chamilo
ADD https://github.com/chamilo/chamilo-lms/releases/download/v${CHAMILO_VERSION}/${CHAMILO_TAR} /var/www/chamilo/chamilo.tar.gz
WORKDIR /var/www/chamilo
RUN tar zxf chamilo.tar.gz;rm chamilo.tar.gz;mv chamilo* www;rm -rf www/tests www/.git* www/.htac*;
WORKDIR /var/www/chamilo/www
RUN chown -R www-data:www-data \
    app \
    main/default_course_document/images \
    main/lang \
    vendor \
    web

# Configure and start Apache
RUN a2dissite 000-default; rm -rf /etc/apache2/sites-enabled/000-default.conf
ADD chamilo.conf /etc/apache2/sites-available/chamilo.conf
RUN a2ensite chamilo; a2enmod rewrite
RUN service apache2 restart

# configure /etc/hosts
RUN echo "127.0.0.1 docker.chamilo.net" >> /etc/hosts

WORKDIR /var/www/chamilo/www
